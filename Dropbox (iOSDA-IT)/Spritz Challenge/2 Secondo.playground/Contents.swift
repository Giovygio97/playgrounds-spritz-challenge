import UIKit
import PlaygroundSupport

class Playground2: UIViewController{
    var egg: UIImageView!
    var hen:[UIImage]! = []
    var button: UIButton!
    var henView: UIImageView!
    
    override func loadView() {
        let view=UIView()
        view.backgroundColor = .white
        
        hen.append(UIImage(named:"Gallinamov1.png")!)
        hen.append(UIImage(named:"gallinamov2.png")!)
        hen.append(UIImage(named:"Gallinamov4.png")!)
        hen.append(UIImage(named:"Gallinamov5.png")!)
        hen.append(UIImage(named:"GallinaUltimo.png")!)
        hen.append(UIImage(named:"Gallivamov3.png")!)
        
        button=UIButton()
        button.setImage(UIImage(named:"button.png"), for: .normal)
        button.frame=CGRect(x: 570, y: 20, width: 50, height: 50)
        button.isUserInteractionEnabled=true
        view.addSubview(button)
        
        egg=UIImageView()
        egg.image=UIImage(named:"uovo1.png")
        egg.frame=CGRect(x: 500, y: 300, width: 70, height: 90)
        view.addSubview(egg)
        
        henView=UIImageView()
        henView.image=hen[0]
        henView.frame=CGRect(x: 210, y: 150, width: 150, height: 250)
        view.addSubview(henView)
        
        let pressione=UITapGestureRecognizer(target: self, action: #selector(azione))
        button.addGestureRecognizer(pressione)
        
        self.view=view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func azione(){
        iteration(imageview:self.henView, imagearray:self.hen, duration:0.7)
    }
    
    func iteration(imageview: UIImageView, imagearray: [UIImage], duration: Double){
        imageview.animationImages=imagearray
        imageview.animationDuration=duration
        imageview.startAnimating()
        movimento()
    }
    
    func movimento(){
        UIView.animate(withDuration: 6.0, animations: {
            let transform=CGAffineTransform(translationX: 250, y: 0)
            self.henView.transform=transform
        }, completion: {(value: Bool) in
            self.henView.stopAnimating()
            self.henView.image = UIImage(named:"cova?.png")
        })
    }
    
}
let vc = Playground2()
vc.preferredContentSize=CGSize(width: 640, height: 420)
PlaygroundPage.current.liveView = vc
