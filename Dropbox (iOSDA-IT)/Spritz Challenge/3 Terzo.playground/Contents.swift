import UIKit
import PlaygroundSupport

class Playground1: UIViewController{
    var egg: [UIImage]! = []
    var eggs: UIImageView!
    var hen:[UIImage]! = []
    var button: UIButton!
    var henView: UIImageView!
    var baloon: UIImageView!
    var crocodrile: UIImageView!
    var testo: UILabel!
    
    override func loadView() {
        let view=UIView()
        view.backgroundColor = .yellow
        
        hen.append(UIImage(named:"Gallinamov1.png")!)
        hen.append(UIImage(named:"gallinamov2.png")!)
        hen.append(UIImage(named:"Gallinamov4.png")!)
        hen.append(UIImage(named:"Gallinamov5.png")!)
        hen.append(UIImage(named:"GallinaUltimo.png")!)
        hen.append(UIImage(named:"Gallivamov3.png")!)
        
        
        
        button=UIButton()
        button.setImage(UIImage(named:"button.png"), for: .normal)
        button.frame=CGRect(x: 570, y: 20, width: 50, height: 50)
        button.isUserInteractionEnabled=true
        view.addSubview(button)
        
        crocodrile=UIImageView()
        crocodrile.frame=CGRect(x: 480, y: 270, width: 70, height: 90)
        view.addSubview(self.crocodrile)
        
        eggs=UIImageView()
        eggs.image=UIImage(named:"uovo1.png")
        eggs.frame=CGRect(x: 500, y: 300, width: 70, height: 90)
        view.addSubview(eggs)
        
        egg.append(UIImage(named:"uovo1.png")!)
        egg.append(UIImage(named:"uovo2.png")!)
        egg.append(UIImage(named:"uovo3.png")!)
        egg.append(UIImage(named:"uovo4.png")!)
        egg.append(UIImage(named:"uovo5.png")!)
        egg.append(UIImage(named:"uovo6.png")!)
        egg.append(UIImage(named:"uovo7.png")!)
        egg.append(UIImage(named:"uovo8.png")!)
        egg.append(UIImage(named:"uovo9.png")!)
        egg.append(UIImage(named:"uovo10.png")!)
        
        henView=UIImageView()
        henView.image=UIImage(named:"cova?.png")
        henView.frame=CGRect(x: 460, y: 150, width: 150, height: 250)
        view.addSubview(henView)
        
        let pressione=UITapGestureRecognizer(target: self, action: #selector(azione))
        button.addGestureRecognizer(pressione)
        
        self.view=view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func azione(){
        iteration(imageview:self.henView, imagearray:self.hen, duration:0.5)
    }
    
    func iteration(imageview: UIImageView, imagearray: [UIImage], duration: Double){
        imageview.animationImages=imagearray
        imageview.animationDuration=duration
        imageview.startAnimating()
        movimento()
    }
    
    func creation_baloon(){
        self.baloon=UIImageView()
        self.testo=UILabel()
        self.baloon.image=UIImage(named:"baloon1.png")
        self.baloon.frame=CGRect(x: 340, y: 130, width: 105, height: 100)
        self.testo.text="Coccodé?"
        self.testo.font=UIFont(name: "San Francisco", size: 40)
        self.testo.textColor = .black
        self.testo.frame=CGRect(x:355,y:150,width: 90,height: 40)
        self.view.addSubview(self.baloon)
        self.view.addSubview(self.testo)
    }
    
    func animation_egg(imagev: UIImageView, array: [UIImage], duration: Double, d: UInt32){
        imagev.animationImages=array
        imagev.animationDuration=duration
        imagev.animationRepeatCount=1
        imagev.startAnimating()
    }
    
    func movimento(){
        UIView.animate(withDuration: 5.0, animations: {
            let transform=CGAffineTransform(translationX: -230, y: 0)
            self.henView.transform=transform
        }, completion: {(value: Bool) in
            self.henView.stopAnimating()
            self.henView.image = self.hen[4]
            self.creation_baloon()
            UIView.animate(withDuration: 1.0, animations: {self.animation_egg(imagev: self.eggs, array: self.egg, duration: 1.0, d:1)}, completion: {
                (value:Bool) in
                self.crocodrile.image=UIImage(named:"croccodillo.png")
                self.eggs.image=UIImage(named:"uovo11.png")
            })
            
        })
    }
    

}
let vc=Playground1()
vc.preferredContentSize=CGSize(width: 640, height: 420)
PlaygroundPage.current.liveView = vc

