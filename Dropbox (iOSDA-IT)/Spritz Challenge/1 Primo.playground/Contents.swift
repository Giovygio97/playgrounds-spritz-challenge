import UIKit
import PlaygroundSupport

class Playground1: UIViewController{
    var egg: UIImageView!
    var hen:[UIImage]! = []
    var button: UIButton!
    var henView: UIImageView!
    var baloon: UIImageView!
    var testo: UILabel!
    
    override func loadView() {
        let view=UIView()
        view.backgroundColor = .yellow
        
        hen.append(UIImage(named:"Gallinamov1.png")!)
        hen.append(UIImage(named:"gallinamov2.png")!)
        hen.append(UIImage(named:"Gallinamov4.png")!)
        hen.append(UIImage(named:"Gallinamov5.png")!)
        hen.append(UIImage(named:"GallinaUltimo.png")!)
        hen.append(UIImage(named:"Gallivamov3.png")!)
        
        
        henView=UIImageView()
        henView.image=hen[0]
        henView.frame=CGRect(x: -150, y: 150, width: 150, height: 250)
        view.addSubview(henView)
        
        button=UIButton()
        button.setImage(UIImage(named:"button.png"), for: .normal)
        button.frame=CGRect(x: 570, y: 20, width: 50, height: 50)
        button.isUserInteractionEnabled=true
        view.addSubview(button)
        
        egg=UIImageView()
        egg.image=UIImage(named:"uovo1.png")
        egg.frame=CGRect(x: 500, y: 300, width: 70, height: 90)
        view.addSubview(egg)
        
        let pressione=UITapGestureRecognizer(target: self, action: #selector(azione))
        button.addGestureRecognizer(pressione)
        
        self.view=view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func azione(){
        iteration(imageview:self.henView, imagearray:self.hen, duration:0.5)
    }
    
    func iteration(imageview: UIImageView, imagearray: [UIImage], duration: Double){
        imageview.animationImages=imagearray
        imageview.animationDuration=duration
        imageview.startAnimating()
        movimento()
    }
    
    func creation_baloon(){
        self.baloon=UIImageView()
        self.testo=UILabel()
        self.baloon.image=UIImage(named:"baloon1.png")
        self.baloon.frame=CGRect(x: 340, y: 130, width: 210, height: 100)
        self.testo.text="It's an egg, Coccodé!"
        self.testo.font=UIFont(name: "San Francisco", size: 40)
        self.testo.textColor = .black
        self.testo.frame=CGRect(x:365,y:150,width: 185,height: 40)
        self.view.addSubview(self.baloon)
        self.view.addSubview(self.testo)
    }
    
    func movimento(){
        UIView.animate(withDuration: 5.0, animations: {
            let transform=CGAffineTransform(translationX: 360, y: 0)
            self.henView.transform=transform
        }, completion: {(value: Bool) in
            self.henView.stopAnimating()
            self.henView.image = self.hen[4]
            self.creation_baloon()
        })
    }
    

}
let vc=Playground1()
vc.preferredContentSize=CGSize(width: 640, height: 420)
PlaygroundPage.current.liveView = vc

